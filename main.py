import email
import smtplib
import time

import pytest
import imaplib

user_login = ''
user_password = ''


def authorization(login, password):
    mail = smtplib.SMTP_SSL('smtp.mail.ru', 465)
    mail.login(login, password)
    return mail


def send_email_helper(login, password, subject, msg):
    email_text = f"From: {login}\r\nTo: {login}\r\nSubject: {subject}\r\n{msg}\r\n\r\n"

    mail = authorization(login, password)
    mail.sendmail(login, login, email_text)
    mail.close()


def check_email_helper(login, password, subject, msg):
    mail = imaplib.IMAP4_SSL('imap.mail.ru', 993)
    mail.login(login, password)
    mail.select('INBOX')
    status, list_of_unread_email = mail.search(None, 'UNSEEN')

    """Check"""

    expected_amount_of_emails = 1
    real_amount_of_suitable_mails = 0

    for unread_email in list_of_unread_email[0].split():
        status, data = mail.fetch(unread_email, '(RFC822)')

        cur_msg = email.message_from_bytes(data[0][1])
        email_subject = cur_msg['Subject']
        email_from = cur_msg['From']
        email_to = cur_msg['To']
        email_text = cur_msg.get_payload()

        if subject.strip().lower() == email_subject.strip().lower() and\
            login.strip().lower() == email_from.strip().lower() and\
            login.strip().lower() == email_to.strip().lower() and\
            msg.strip().lower() == email_text.strip().lower() :
                real_amount_of_suitable_mails += 1

    mail.close()
    # Проверяем что из всех непрочитанных сообщений пришло ровно 1 , нами отправленное сообщение.
    # Возможно лучше еще проверять дату.
    assert expected_amount_of_emails == real_amount_of_suitable_mails

@pytest.mark.xfail
@pytest.mark.parametrize("login, password",
                         [(user_login, '111')])
def test_bad_auth_to_email(login, password):
    authorization(login, password)


@pytest.mark.parametrize("login, password",
                         [(user_login, user_password)])
def test_good_auth_to_email(login, password):
    authorization(login, password)


@pytest.mark.parametrize("login, password, subject, msg",
                         [(user_login, user_password, 'test1', 'This is test email')])
def test_send_and_check_email(login, password, subject, msg):
    send_email_helper(login, password, subject, msg)
    time.sleep(3)  ## Надо подождать пока появится сообщение на почте
    check_email_helper(login, password, subject, msg)


if __name__ == "__main__":
    pass

## Проверить на фейковом gmail аккаунте
# content = 'test message'
# mail = smtplib.SMTP_SSL('smtp.gmail.com', 465)
# mail.login(login,password)
# mail.sendmail(login, login, content)
# mail.close()
